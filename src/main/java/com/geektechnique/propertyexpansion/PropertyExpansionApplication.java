package com.geektechnique.propertyexpansion;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class PropertyExpansionApplication {

    public static void main(String[] args) {
        SpringApplication.run(PropertyExpansionApplication.class, args);
    }

}
